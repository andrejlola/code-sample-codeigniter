<?php

/**
 * Class MY_Model
 */
class MY_Model extends CI_Model {
    const STRIP_PATTERN = '/[^A-Za-z0-9]/';

    private $sql;
    private $bind_marker;

    /**
     * @inheritdoc
     */
    function  __construct() {
        parent::__construct();
        $this->load->database();
        $this->bind_marker = $this->db->bind_marker;
    }

    /**
     * Delets record from table
     *
     * @param $id
     */
    function delete_by_id ($id) {
        $this->db->delete($this->_table_name, array('id' => $id));
    }

    /**
     * Inserts record into table
     *
     * @param $data
     * @return mixed
     */
    function insert($data) {
        $this->db->insert($this->_table_name, $data);

        return $this->db->insert_id();
    }

    /**
     * Updates record in table
     *
     * @param $id
     * @param $data
     * @return mixed
     */
    function update_by_id($id, $data) {
        $this->db->where('id', $id);
        $this->db->update($this->_table_name, $data);

        return true;
    }

    /**
     * Get record from table
     *
     * @param $id
     * @param bool $use_master
     * @return bool
     */
    function get_by_id($id, $use_master = false) {
        if ($use_master) {
            $this->db->use_master = true;
        }
        $sets = array(
            'where' => array(
                't.id' => $id
            )
        );
        $new = $this->get_slice($sets);
        if(isset($new[0])) {
            return $new[0];
        }

        return false;
    }

    /**
     * Execute an query
     *
     * @param $sql
     * @param bool $bind
     * @param bool $return_object
     * @return mixed
     */
    final protected function query($sql, $bind=false, $return_object = true) {
        $this->sql = $sql;
        if(is_array($bind) and count($bind)>0){
            $bind = $this->process_bind($bind);
        }
        $query = $this->db->query($this->sql, $bind, $return_object);

        return $query;
    }

    /**
     * Binds rows
     *
     * @param $bind
     * @return array|null
     */
    private function process_bind($bind) {
        $bindOrder = null;
        $bindList = null;
        $pattern = "/[^']:[A-Za-z0-9_]+[^']/";
        $preg = preg_match_all($pattern, $this->sql, $matches, PREG_OFFSET_CAPTURE);
        if ($preg !== 0 and $preg !== false) {
            foreach ($matches[0] as $key => $val) {
                $bindOrder[$key] = trim($val[0]);
            }
            foreach ($bindOrder as $field) {
                $this->sql = str_replace($field, $this->bind_marker, $this->sql);
                $bindList[] = $bind[$field];
            }
        } else {
            $bindList = $bind;
        }

        return $bindList;
    }
}
