<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?php echo $title; ?></title>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <link href="<?php echo site_url('/css/bootstrap.min.css'); ?>" rel="stylesheet" />
    <link href="<?php echo site_url('/css/sb-admin.css'); ?>" rel="stylesheet" />
    <link href="<?php echo site_url('/css/plugins/morris.css'); ?>" rel="stylesheet" />
    <link href="<?php echo site_url('/font-awesome-4.1.0/css/font-awesome.min.css'); ?>" rel="stylesheet" />
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo site_url('manage/index'); ?>">Admin</a>
            </div>

            <ul class="nav navbar-right top-nav">
                <li class="dropdown">

                </li>
            </ul>

            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="<?php echo $this->action_name == 'index' ? 'active' : ''; ?>">
                        <a href="<?php echo site_url('manage/index'); ?>"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li class="<?php echo $this->action_name == 'adverts_for_approval' ? 'active' : ''; ?>">
                        <a href="<?php echo site_url('manage/adverts_for_approval'); ?>"><i class="fa fa-fw fa-picture-o"></i> Adverts</a>
                    </li>
                    <li class="<?php echo $this->action_name == 'adverts_statistic' ? 'active' : ''; ?>">
                        <a href="<?php echo site_url('manage/adverts_statistic'); ?>"><i class="glyphicon glyphicon-stats"></i> Adverts Statistic</a>
                    </li>
                    <li class="<?php echo $this->action_name == 'payments' ? 'active' : ''; ?>">
                        <a href="<?php echo site_url('manage/payments'); ?>"><i class="glyphicon glyphicon-stats"></i> Payments</a>
                    </li>
                    <li>
                        <a href="<?php echo get_logout_url(); ?>"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                    </li>
                </ul>
            </div>
        </nav>

        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            <?php echo $title; ?>
                        </h1>
                    </div>
                </div>
                <div>
                    <?php echo $content; ?>
                </div>
            </div>
        </div>
    </div>
    <script src="<?php echo site_url('/js/jquery.js'); ?>"></script>
    <script src="<?php echo site_url('/js/bootstrap.min.js'); ?>"></script>
</body>
</html>
