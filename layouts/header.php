<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Admin</title>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <link href="<?php echo site_url('/css/bootstrap.min.css'); ?>" rel="stylesheet" />
    <link href="<?php echo site_url('/css/sb-admin.css'); ?>" rel="stylesheet" />
    <link href="<?php echo site_url('/css/plugins/morris.css'); ?>" rel="stylesheet" />
    <link href="<?php echo site_url('/font-awesome-4.1.0/css/font-awesome.min.css'); ?>" rel="stylesheet" />
</head>
<body>
    <div id="wrapper">
