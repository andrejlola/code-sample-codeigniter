<?php

/**
 * Class Payment_Model
 */
class Payment_Model extends MY_Model
{
    public $_table_name = 'payment';
    private $bt_helper;

    /**
     * @inheritdoc
     */
    public function __construct()
    {
        $this->bt_helper = new BTHelper();
    }

    /**
     * Makes test payment
     * @param $user_id
     * @return bool|object
     */
    function test_payment($user_id)
    {
        $payment_data = $this->pay($user_id, TEST_PAYMENT_AMOUNT);
        if ($payment_data) {
            return (object)$payment_data;
        }

        return false;
    }

    /**
     * Makes payment
     *
     * @param $advert
     * @return bool|object
     */
    function pay_for_advert($advert)
    {
        $payment_data = $this->pay($advert->user_id, $advert->cost, $advert->id);
        if ($payment_data) {
            $payment_data['business_name'] = $advert->business_name;
            $payment_data['user_email'] = $advert->user_email;

            return (object)$payment_data;
        }

        return false;
    }

    /**
     * Payment processing
     *
     * @param $user_id
     * @param $amount
     * @param null $advert_id
     * @return array|bool
     */
    private function pay($user_id, $amount, $advert_id = null)
    {
        $payment = $this->bt_helper->transaction_sale($user_id, $amount);
        if (!$payment->success) {
            $message = array();
            foreach ($payment->errors->deepAll() AS $error) {
                $message[] = $error->message;
            }
            $message = implode(';', $message);
        } else {
            $message = '';
        }

        $transaction_id = '';
        $merchantAccountId = '';
        $processorAuthorizationCode = '';
        $processorResponseCode = '';
        $processorResponseText = '';
        $token = '';
        $bin = '';
        $last4 = '';
        $expirationMonth = '';
        $expirationYear = '';
        $customerLocation = '';
        $uniqueNumberIdentifier = '';

        if (isset($payment->transaction->id) && !is_null($payment->transaction->id)) {
            $transaction_id = $payment->transaction->id;
        }
        if (isset($payment->transaction->merchantAccountId) && !is_null($payment->transaction->merchantAccountId)) {
            $merchantAccountId = $payment->transaction->merchantAccountId;
        }
        if (isset($payment->transaction->processorAuthorizationCode) && !is_null($payment->transaction->processorAuthorizationCode)) {
            $processorAuthorizationCode = $payment->transaction->processorAuthorizationCode;
        }
        if (isset($payment->transaction->processorResponseCode) && !is_null($payment->transaction->processorResponseCode)) {
            $processorResponseCode = $payment->transaction->processorResponseCode;
        }
        if (isset($payment->transaction->processorResponseText) && !is_null($payment->transaction->processorResponseText)) {
            $processorResponseText = $payment->transaction->processorResponseText;
        }
        if (isset($payment->transaction->creditCard['token']) && !is_null($payment->transaction->creditCard['token'])) {
            $token = $payment->transaction->creditCard['token'];
        }
        if (isset($payment->transaction->creditCard['bin']) && !is_null($payment->transaction->creditCard['bin'])) {
            $bin = $payment->transaction->creditCard['bin'];
        }
        if (isset($payment->transaction->creditCard['last4']) && !is_null($payment->transaction->creditCard['last4'])) {
            $last4 = $payment->transaction->creditCard['last4'];
        }
        if (isset($payment->transaction->creditCard['expirationMonth']) && !is_null($payment->transaction->creditCard['expirationMonth'])) {
            $expirationMonth = $payment->transaction->creditCard['expirationMonth'];
        }
        if (isset($payment->transaction->creditCard['expirationYear']) && !is_null($payment->transaction->creditCard['expirationYear'])) {
            $expirationYear = $payment->transaction->creditCard['expirationYear'];
        }
        if (isset($payment->transaction->creditCard['customerLocation']) && !is_null($payment->transaction->creditCard['customerLocation'])) {
            $customerLocation = $payment->transaction->creditCard['customerLocation'];
        }
        if (isset($payment->transaction->creditCard['uniqueNumberIdentifier']) && !is_null($payment->transaction->creditCard['uniqueNumberIdentifier'])) {
            $uniqueNumberIdentifier = $payment->transaction->creditCard['uniqueNumberIdentifier'];
        }

        $payment_data = array(
            'advert_id' => $advert_id,
            'user_id' => $user_id,
            'transaction_id' => $transaction_id,
            'success' => $payment->success,
            'message' => $message,
            'amount' => $amount,
            'merchant_account_id' => $merchantAccountId,
            'processor_autorization_code' => $processorAuthorizationCode,
            'processor_response_code' => $processorResponseCode,
            'processor_response_text' => $processorResponseText,
            'card_token' => $token,
            'card_bin' => $bin,
            'card_last4' => $last4,
            'card_expiration_month' => $expirationMonth,
            'card_expiration_year' => $expirationYear,
            'card_customer_location' => $customerLocation,
            'card_unique_number_identifier' => $uniqueNumberIdentifier,
        );

        $id = $this->insert($payment_data);
        if ($id > 0) {
            return (array)$payment_data;
        } else {
            return false;
        }
    }

    /**
     * Returns models list of payments
     *
     * @param $filter
     * @param int $offset
     * @param int $limit
     * @return mixed
     */
    function get_all($filter, $offset = 0, $limit = 999999)
    {
        $this->db->select('a.business_name, u.email, p.*');
        $this->db->from($this->_table_name . ' as p');
        if (isset($filter['from'])) {
            $this->db->where('time_added >=', $filter['from']);
        }
        if (isset($filter['to'])) {
            $this->db->where('time_added <=', $filter['to']);
        }
        $this->db->join('users as u', 'u.id = p.user_id');
        $this->db->join('advertise as a', 'a.id = p.advert_id', 'left');
        $this->db->order_by('p.id', 'DESC');

        return $this->db->get()->result();
    }
}
