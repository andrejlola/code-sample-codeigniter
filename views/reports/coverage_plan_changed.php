<h1><?php echo $title ?></h1>

<p>
    Coverage plan changed
</p>
<p>
    Details:
</p>
<table>
    <thead>
    <tr>
        <td>
            Business name
        </td>
        <td>
            Old tariff plan
        </td>
        <td>
            New tariff plan
        </td>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($adverts as $advert) : ?>
        <tr>
            <td>
                <?php echo $advert->business_name; ?>
            </td>
            <td>
                <?php echo $advert->coverage_plan_old; ?>
            </td>
            <td>
                <?php echo $advert->coverage_plan_new; ?>
            </td>
        </tr>
    <?php endforeach ?>
    </tbody>
</table>