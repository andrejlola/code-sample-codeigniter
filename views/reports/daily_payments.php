<!DOCTYPE html>
<html>
    <head>
        <style type="text/css">
<!--            --><?php //echo $css; ?>
        </style>
        <title><?php echo $title; ?></title>
    </head>
    <body>
        <h1><?php echo $title; ?></h1>
        <div class="col-lg-12">
            <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr>
                            <th class="text-center">Business Name</th>
                            <th class="text-center">Amount</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Message</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($rows as $row) {?>
                        <tr>
                            <td><?php echo $row->business_name; ?></td>
                            <td class="text-center"><?php echo $row->amount; ?></td>
                            <td class="text-center"><?php echo $row->success ? '+' : '-'; ?></td>
                            <td class="text-center"><?php echo $row->message; ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>
