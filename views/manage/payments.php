<div class="row">
    <div class="col-lg-2">
        <form action="<?php echo site_url('manage/payments'); ?>" method="post" id="form_filter">
            <input id="filter" name="filter" type="text" placeholder="Business Name" value="<?php echo empty($filter) ? '' : $filter; ?>">
            <input id="clear_filter" name="clear_filter" type="hidden" value="0">
            <button class="btn btn-sm btn-primary" type="submit"><i class="glyphicon glyphicon-filter"></i> Filter</button>
        </form>
    </div>
    <div class="col-lg-1">
        <button class="btn btn-sm btn-primary" type="button" onclick="btnClearFilterClick();"><i class="glyphicon glyphicon-ban-circle"></i> Clear Filter</button>
    </div>
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped">
                <thead>
                    <tr>
                        <th class="text-center">Business Name</th>
                        <th class="text-center">User Email</th>
                        <th class="text-center">Card Number</th>
                        <th class="text-center">Card Expiration</th>
                        <th class="text-center">Date</th>
                        <th class="text-center">Transaction ID</th>
                        <th class="text-center">Status</th>
                        <th class="text-center">Message</th>
                        <th class="text-center">Amount</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($rows as $row) {?>
                    <tr <?php echo !$row->success ? 'class="danger"' : '' ?> >
                        <td><?php echo $row->business_name; ?></td>
                        <td class="text-center"><?php echo $row->email; ?></td>
                        <td class="text-center"><?php echo $row->card_bin.'******'.$row->card_last4; ?></td>
                        <td class="text-center"><?php echo $row->card_expiration_month.'/'.$row->card_expiration_year; ?></td>
                        <td class="text-center"><?php echo $row->time_add; ?></td>
                        <td class="text-center"><?php echo $row->transaction_id; ?></td>
                        <td class="text-center"><?php echo $row->success ? '+' : '-'; ?></td>
                        <td class="text-center"><?php echo $row->message; ?></td>
                        <td class="text-center"><?php echo $row->amount; ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<nav>
    <ul class="pagination">
        <?php echo $pagination_helper->create_links(); ?>
    </ul>
</nav>
<script type="text/javascript">
    function btnClearFilterClick() {
        $('#clear_filter').val(1);
        $('#form_filter').submit();
    }
</script>
