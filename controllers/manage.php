<?php

/**
 * Class manage
 */
class manage extends CI_Controller
{
    protected $data = array();
    public $controller_name;
    public $action_name;
    public $view_path;

    /**
     * @inheritdoc
     */
    function __construct()
    {
        parent::__construct();
        $this->controller_name = $this->router->fetch_directory() . $this->router->fetch_class();
        $this->action_name = $this->router->fetch_method();
        $this->view_path = $this->controller_name . '/' . $this->action_name . '.php';
        $this->data['title'] = 'Admin';
        $this->data['content'] = '';
        $this->load->model('advertise_model', 'advertise_model');
    }

    /**
     * @link http://site.com/dashboard Dashboard home page
     */
    function index()
    {
        $this->data['title'] = 'DashBoard';

        $count_for_approval = $this->advertise_model->get_count_for_approval();
        $count_declined = $this->advertise_model->get_count_declined();
        $count_approved = $this->advertise_model->get_count_approved();

        $this->data['dash_board'] = array(
            'unapproved_adverts' => array(
                'name' => 'Adverts',
                'count_for_approval' => $count_for_approval,
                'count_declined' => $count_declined,
                'count_approved' => $count_approved,
            )
        );

        $this->render();
    }

    /**
     * @link http://site.com/dashboard/adverts-for-approval Adverts for approval list and manage page
     */
    function adverts_for_approval()
    {
        $this->data['title'] = 'Adverts For Approval';

        $items_per_page = 3;
        $params = $this->input->post();
        if ($this->input->is_ajax_request() && $params && isset($params['id']) && isset($params['action'])) {
            switch ($params['action']) {
                case 'approve':
                    $this->advertise_model->advert_approve($params['id'], $params['action']);
                    break;
                case 'decline':
                    $this->advertise_model->advert_decline($params['id'], $params['action']);
                    break;
                case 'remove':
                    $this->advertise_model->advertise_delete($params['id']);
                    break;
            }
            $this->data['rows'] = $this->advertise_model->get_adverts_for_approval(0, $items_per_page);
            echo $this->load->view($this->view_path, $this->data, true);
            return;
        }

        $this->data['rows'] = $this->advertise_model->get_adverts_for_approval(0, $items_per_page);
        $this->render();
    }

    /**
     * @link http://site.com/dashboard/adverts-declined Declined adverts page
     */
    function adverts_declined($page = 1)
    {
        $this->data['title'] = 'Declined Adverts';

        $this->load->library('pagination');
        $this->load->library('paginationlib');

        $pagingConfig = $this->paginationlib->initPagination($this->controller_name . '/' . $this->action_name,
            $this->advertise_model->get_count_declined());
        $this->data['pagination_helper'] = $this->pagination;
        $this->data['rows'] = $this->advertise_model->get_adverts_declined((($page - 1) * $pagingConfig['per_page']),
            $pagingConfig['per_page']);

        $this->render();
    }

    /**
     * @link http://site.com/dashboard/adverts-approved Approved adverts page
     */
    function adverts_approved($page = 1)
    {
        $this->data['title'] = 'Approved';

        $this->load->library('pagination');
        $this->load->library('paginationlib');

        $pagingConfig = $this->paginationlib->initPagination($this->controller_name . '/' . $this->action_name,
            $this->advertise_model->get_count_approved());
        $this->data['pagination_helper'] = $this->pagination;
        $this->data['rows'] = $this->advertise_model->get_adverts_approved((($page - 1) * $pagingConfig['per_page']),
            $pagingConfig['per_page']);

        $this->render();
    }

    /**
     * @link http://site.com/dashboard/adverts-statistic Adverts statistic page
     */
    function adverts_statistic($page = 1)
    {
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->library('paginationlib');

        $this->data['title'] = 'Adverts Statistic';

        $clear_filter = $this->input->get_post('clear_filter');
        if ($clear_filter == 1) {
            $this->searchterm_clear();
            $filter = null;
        } else {
            $filter = $this->input->get_post('filter', true);
        }

        $filter = $this->searchterm_handler($filter);
        $this->data['filter'] = $filter;

        $pagingConfig = $this->paginationlib->initPagination($this->controller_name . '/' . $this->action_name,
            count($this->advertise_model->get_statistic_admin($filter)));
        $this->data['pagination_helper'] = $this->pagination;
        $this->data['rows'] = $this->advertise_model->get_statistic_admin($filter,
            (($page - 1) * $pagingConfig['per_page']), $pagingConfig['per_page']);

        $this->render();
    }

    /**
     * @link http://site.com/dashboard/payments Payments page
     */
    function payments($page = 1)
    {
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->library('paginationlib');
        $this->load->model('payment_model', 'payment_model');

        $this->data['title'] = 'Payments';

        $clear_filter = $this->input->get_post('clear_filter');
        if ($clear_filter == 1) {
            $this->searchterm_clear();
            $filter = null;
        } else {
            $filter = $this->input->get_post('filter', true);
        }

        $filter = $this->searchterm_handler($filter);
        $this->data['filter'] = $filter;

        $pagingConfig = $this->paginationlib->initPagination($this->controller_name . '/' . $this->action_name,
            count($this->payment_model->get_all($filter)));
        $this->data['pagination_helper'] = $this->pagination;
        $this->data['rows'] = $this->payment_model->get_all($filter, (($page - 1) * $pagingConfig['per_page']),
            $pagingConfig['per_page']);

        $this->render();
    }

    /**
     * Wrapper
     */
    protected function render()
    {
        if (file_exists(APPPATH . 'views/' . $this->view_path)) {
            $this->data['content'] .= $this->load->view($this->view_path, $this->data, true);
        }
        $this->load->view('layouts/manage.php', $this->data);
    }
}
